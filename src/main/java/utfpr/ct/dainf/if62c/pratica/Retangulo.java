/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author kelvin
 */
public class Retangulo implements FiguraComLados {
    double base;
    double altura;
    public Retangulo(){
        
    }
    public Retangulo(double base,double altura){
        this.base = base;
        this.altura = altura;
    }
    public double getArea(){
        double area;
        area = this.base * this.altura;
        return area;
    }
    public double getPerimetro(){
        double perimetro;
        perimetro = (this.base + this.altura) * 2;
        return perimetro;
    }
    public double getLadoMaior(){
        return this.base;
    }
    public double getLadoMenor(){
        return this.altura;
    }
}
