/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author kelvin
 */
public class  Elipse implements FiguraComEixos {
    private double semieixo1;
    private double semieixo2;
    private String nome;
    public Elipse(){
        
    }
    public Elipse(double semieixo1,double semieixo2){
        this.semieixo1 = semieixo1;
        this.semieixo2 = semieixo2;
    }
    public String getNome(){
        return this.nome = "Elipse";
    }
    public double getEixoMaior(){
        return this.semieixo1;
    }
    public double getEixoMenor(){
        return this.semieixo2;
    }
    public double  getArea(){
        double area;
        area = Math.PI * this.semieixo1 * this.semieixo2;
        return area;
    }
    public double getPerimetro(){
        double perimetro;
        perimetro = Math.PI * (3 * (this.semieixo1 + this.semieixo2) - Math.sqrt(((3 * this.semieixo1) + this.semieixo2) * (this.semieixo1 + (3 * this.semieixo2))));
        return perimetro;
    }
}
