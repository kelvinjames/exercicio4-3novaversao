/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author kelvin
 */
public class TrianguloEquilatero extends Retangulo implements FiguraComLados {
    public TrianguloEquilatero(){
    }
    public TrianguloEquilatero(double lado){
        super(lado,lado * Math.sqrt(3) /2);
    }
    @Override
    public double getArea(){
        double area;
        area = super.getArea()/2;
        return area;
    }
    @Override
    public double getPerimetro(){
        double perimetro;
        perimetro = 3 * this.base;
        return perimetro;
    }
    @Override
    public double getLadoMaior(){
        return this.base;
    }
    @Override
    public double getLadoMenor(){
        return this.altura;
    }
}
