/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author kelvin
 */
public class Circulo extends Elipse {
    double raio;
    public Circulo(){
    }
    public Circulo(double raio){
        this.raio = raio;
    }
    public Circulo(double semieixo1,double semieixo2){
        super(semieixo1,semieixo2);
    }
    public Circulo(double semieixo1,double semieixo2,double raio){
        super(semieixo1,semieixo2);
        this.raio = raio;
    }
    @Override
    public double getPerimetro(){
        double perimetro;
        perimetro = 2 * Math.PI * this.raio;
        return perimetro;
    }
}
