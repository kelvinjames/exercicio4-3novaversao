/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author kelvin
 */
public class Quadrado extends Retangulo implements FiguraComLados{
    public Quadrado(){
        
    }
    public Quadrado(double lado){
       super(lado,lado);
    }
    public double getArea(){
        double area;
        area = super.getArea();
        return area;
    }
    public double getPerimetro(){
        double perimetro;
        perimetro = 4 * this.base;
        return perimetro;
    }
    public double getLadoMaior(){
        return this.base;
    }
    public double getLadoMenor(){
        return this.altura;
    }
}
