
import utfpr.ct.dainf.if62c.pratica.Circulo;
import utfpr.ct.dainf.if62c.pratica.Elipse;
import utfpr.ct.dainf.if62c.pratica.Quadrado;
import utfpr.ct.dainf.if62c.pratica.Retangulo;
import utfpr.ct.dainf.if62c.pratica.TrianguloEquilatero;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica43 {
    public static void main(String[] args) {
        Elipse elipse = new Elipse(5,5);
        Circulo circulo1 = new Circulo(5);
        Circulo circulo2 = new Circulo(5,5);
        Retangulo retangulo = new Retangulo(3,4);
        Quadrado quadrado = new Quadrado(1);
        TrianguloEquilatero trianguloequilatero = new TrianguloEquilatero(2);
        System.out.println(elipse.getArea());
        System.out.println(elipse.getPerimetro());
        System.out.println(circulo2.getArea());
        System.out.println(circulo1.getPerimetro());
        System.out.println(retangulo.getArea());
        System.out.println(retangulo.getPerimetro());
        System.out.println(quadrado.getArea());
        System.out.println(quadrado.getPerimetro());
        System.out.println(trianguloequilatero.getArea());
        System.out.println(trianguloequilatero.getPerimetro());
    }
}
